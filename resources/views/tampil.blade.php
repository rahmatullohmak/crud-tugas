@extends('master')
@section('halaman')

tampil
@endsection


@section('content')
  @forelse ($data as $key=> $tampil)
  <div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">{{ $tampil->nama }}</h5>
    <h6 class="card-subtitle mb-2 text-body-secondary">{{ $tampil->alamat }}</h6>
    <p class="card-text">{{ $tampil->bio }}</p>
    <a href="/cast/create" class="card-link">Card link</a>
    <a href="/cast/{{ $tampil->id }}" class="card-link">detile</a>
<form action="/cast/{{ $tampil->id }}" method="post">
@csrf
@method('delete')
    <input type="submit" value="hapus">
</form>
  </div>
</div>
@empty
    <p>No users</p>
@endforelse
@endsection
